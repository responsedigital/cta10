module.exports = {
    'name'  : 'CTA10',
    'camel' : 'CTA10',
    'slug'  : 'cta-10',
    'dob'   : 'CTA_10_1440',
    'desc'  : 'A simple contact section with a short description, phone, email, and social icons.',
}