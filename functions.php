<?php

namespace Fir\Pinecones\CTA10;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'CTA10',
            'label' => 'Pinecone: CTA10',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A simple contact section with a short description, phone, email, and social icons."
                ],
                [
                    'label' => 'Content',
                    'name' => 'contentTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text'
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea'
                ],
                [
                    'label' => 'Phone',
                    'name' => 'phone',
                    'type' => 'text'
                ],
                [
                    'label' => 'Email',
                    'name' => 'email',
                    'type' => 'text'
                ],
                [
                    'label' => 'Social',
                    'name' => 'socialTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Facebook',
                    'name' => 'facebook',
                    'type' => 'link'
                ],
                [
                    'label' => 'Twitter',
                    'name' => 'twitter',
                    'type' => 'link'
                ],
                [
                    'label' => 'Instagram',
                    'name' => 'instagram',
                    'type' => 'link'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
