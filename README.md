![Pinecone: CTA 10](https://res.cloudinary.com/fir-design/image/upload/v1595351780/Pinecones/pinecone-cta-10.png)

Pinecone: CTA10

DOB Reference: CTA_10_1440

Description: A simple contact section with a short description, phone, email, and social icons.