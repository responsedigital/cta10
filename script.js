class CTA10 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initCTA10()
    }

    initCTA10 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: CTA10")
    }

}

window.customElements.define('fir-cta-10', CTA10, { extends: 'div' })
